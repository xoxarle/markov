package markov

import (
	"fmt"
	"testing"
)

func TestMarkov(t *testing.T) {
	t.Run("Check wordlength must be != 0", func(t *testing.T) {
		m := InitMarkovStruct()
		i := 0
		if m.WordLen == i {
			t.Errorf("Should get wordlen != 0, instead got %d", m.WordLen)
		}
	})
	t.Run("Chain must not be empty", func(t *testing.T) {
		m := InitMarkovStruct()
		m.MakeChain()
		if len(m.Chain) == 0 {
			t.Error("Chain must not be of zero length")
		}
	})
	t.Run("Random ring must not be an empty Couple", func(t *testing.T) {
		m := InitMarkovStruct()
		m.MakeChain()
		for w := 0; w < 100; w++ {
			c := m.RandomRing()
			if c.W1 == "" || c.W2 == "" {
				t.Fatalf("RandomRing must not return an empty couple")
			} else {
			}
		}
	})

	t.Run("Testing phrase generation", func(t *testing.T) {
		m := InitMarkovStruct()
		m.MakeChain()
		md := MarkovDown{Generator: m}
		_, err := md.Phrase(-4, false)
		if err == nil {
			t.Error("Phrase must only accept a number of word > 0")
		}

		s, err := md.Phrase(4, false)
		if s == "" && err == nil {
			t.Errorf("Phrase(4) must not return an empty string without error. Returned %s", s)
		}

	})

	t.Run("Testing full markdown post", func(t *testing.T) {
		m := InitMarkovStruct()
		m.MakeChain()
		md := MarkovDown{Generator: m}
		sData, err := md.MarkdownPost()
		fmt.Println(sData)
		if err != nil {
			fmt.Println(err)
		}

	})

	t.Run("Testing reading from file", func(t *testing.T) {
		fmt.Println("LOADING DATA FROM FILE")
		_, err := InitMarkovFromTextFile("")
		if err == nil {
			t.Error("Calling InitMarkovFromTextFile with an empty param must return an err")
		}
		_, err = InitMarkovFromTextFile("~/does-not-exists.txt")
		if err == nil {
			t.Error("Calling InitMarkovFromTextFile with a non existing filename must return err")
		}
		m := CreateMarkov().PreserveCase().PreservePunctuation()
		m, err = m.LoadTextFile("/home/max/corpus.txt")
		if err != nil {
			t.Errorf("Calling InitMarkovFromTextFile with an existing file must not return an error %s", err)
		}
		m.MakeChain()
		if m == nil {
			t.Error("Calling InitMarkovFromTextFile with an existing file must return a valid Markov chain")
		}
		md := MarkovDown{Generator: m}
		sData, err := md.MarkdownPost()
		fmt.Println(sData)

		s, err := md.Para(4, true)
		if len(s) == 0 {
			t.Errorf("A valid Markov object must return a valid phrase %s", s)

		}

	})

}
